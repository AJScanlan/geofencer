package ie.ajax.geofencer;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, ResultCallback<Status> {

    private List<LatLng> coords = new ArrayList<>(5);
    private List<Geofence> geofences = new ArrayList<>(5);

    private PendingIntent pendingIntent;

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        coords.add(new LatLng(53.347199, -6.2635)); // Tapadoo
        coords.add(new LatLng(53.348046, -6.262363)); // Wigwam
        coords.add(new LatLng(40.7127837, -74.00594130000002)); // NY
        coords.add(new LatLng(25.2048493, 55.270782800000006)); // Dubai
        coords.add(new LatLng(35.6894875, 139.69170639999993)); // Tokyo

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Create an instance of GoogleAPIClient.
        if (client == null) {
            client = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        client.connect();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMyLocationEnabled(true);

        for (int i = 0; i < coords.size(); i++) {
            googleMap.addMarker(new MarkerOptions()
                    .position(coords.get(i))
                    .title("Marker: " + i));
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        setupGeofenceList();

        LocationServices.GeofencingApi.addGeofences(
                client,
                getGeofencingRequest(),
                getGeofencePendingIntent()
        ).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("ALEX", "Connection suspended");
    }

    @Override
    public void onResult(@NonNull Status status) {
        Toast.makeText(this, status.toString(), Toast.LENGTH_LONG).show();
    }

    private void setupGeofenceList() {
        for (int i = 0; i < coords.size(); i++) {
            Geofence fence = new Geofence.Builder()
                    // Set the request ID of the geofence. This is a string to identify this geofence.
                    .setRequestId("Request ID: " + i)
                    .setCircularRegion(
                            coords.get(i).latitude,
                            coords.get(i).longitude,
                            50
                    )
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                    .build();

            geofences.add(fence);

            Log.d("ALEX", fence.toString());
        }
    }

    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(geofences);
        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.
        if (pendingIntent != null) {
            return pendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().

        pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;
    }

}
