package ie.ajax.geofencer;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * @author Alexander Scanlan
 * @since 13/10/2016
 */
public class GeofenceTransitionsIntentService extends IntentService {

    public GeofenceTransitionsIntentService() {
        super("");
    }

    public GeofenceTransitionsIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.d("ALEX", "in onHandleIntent");

        if (intent != null) {

            sendNotification(this, 1, R.mipmap.ic_launcher, "BEHOLD", "geofence magic", "it works");

        }
    }

    public static void sendNotification(Context context, int id, int icon, String tickerText, String contentTitle, String contentText) {

        PendingIntent intent = PendingIntent.getActivity(context, id, new Intent(), 0);
        Notification notification = new Notification.Builder(context).setContentTitle(contentTitle)
                .setContentText(contentText).setTicker(tickerText).setSmallIcon(icon).setAutoCancel(true)
                .setContentIntent(intent).getNotification();

        sendNotification(context, id, notification);
    }

    public static void sendNotification(Context context, int id, Notification notification) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(id, notification);
    }
}
